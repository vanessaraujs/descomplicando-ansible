#### Fases do Projeto

- Provisioning -> criar as instâncias para o cluster (Vagrant);
- Install-k8s -> Instalação do cluster;
- Deploy-app -> Deploy de uma aplicação;
- Monitoring -> Prometheus + Grafana.
